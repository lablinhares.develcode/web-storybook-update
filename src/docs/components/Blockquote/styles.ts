import styled from 'styled-components'


export const BlockquoteRoot = styled.blockquote`
  font-size: .875rem;
  line-height: 1.5625rem;

  background-color: rgba(255, 0, 0, 0.2);
`