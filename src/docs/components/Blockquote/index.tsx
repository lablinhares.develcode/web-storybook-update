import React from 'react'

import { BlockquoteRoot } from './styles'

type BlockquoteProps = React.ComponentProps<'blockquote'>

export const Blockquote: React.FC<BlockquoteProps> = ({ ...props }) => {
  return <BlockquoteRoot {...props} />
}
