import { addons } from '@storybook/manager-api'
import pulsoTheme from './themes/pulso'

addons.setConfig({
  theme: pulsoTheme,
})