import { create } from '@storybook/theming/create'

export default create({
  base: "dark",
  brandTitle: "Pulso",
  brandUrl: "https://pulso.rd.com.br",
  brandImage: "./logo.svg",
});